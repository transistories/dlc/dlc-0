# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2019-11-26

### Added

- Logisim simulation
- Example programs
  - counter
  - fibonacci sequence
- Simple Assembler

[1.0.0]: https://git.transistories.org/transistories/dlc/dlc-0/-/tree/v1.0.0
