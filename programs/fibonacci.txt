// Program to output fibonacci numbers

0   LDA 15  ; Load the ACC with the value at address 15
1   SWP     ; Swap the value in ACC and SWP
2   LDA 15  ; Load the ACC with the value at address 15
3   OUT     ; Output the value in the ACC
4   ADS     ; Add the SWP to the ACC
5   JPO 0   ; Jump on overflow to address 0
6   OUT     ; Output the value of the ACC
7   SWP     ; Swap the value of the ACC and the SWP
8   JMP 4   ; Jump to instruction 4

-- Memory --
A Code	Hex
0 LDA   4
1 15    f
2 SWP   5
3 LDA   4
4 15    f
5 OUT   3
6 ADS   e
7 JPO   2
8 0     0
9 OUT   3
a SWP   5
b JMP   1
c 6     6
d       0
e       0
f 1     1
