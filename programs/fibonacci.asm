.start
    LDA #0x1    ; Load 1 into the ACC register
    SWP         ; Swap ACC and SWP registers
    LDA #0x1    ; Load 1 into the ACC register
    OUT         ; Output the value of the ACC
.main
    ADS         ; Add the SWP value to the ACC value
    JPO start   ; Jump to start on overflow (reset)
    OUT         ; Output the value of the ACC
    SWP         ; Swap the ACC and SWP register
    JMP main    ; Jump back to main (loop)
