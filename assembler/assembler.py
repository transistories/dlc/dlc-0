"""
DLC-1 assembler

Basic command-line tool to assemble programs for the DLC-1 computer.

Copyright (C) 2019  Transistories / DLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

############################################# Imports ##############################################

import argparse

######################################### Global variables #########################################

__author__ = 'P. Cassiman'
__version__ = '1.0.0'

############################################# Classes ##############################################

############################################ Functions #############################################

############################################### Main ###############################################

if __name__ == "__main__":
    # Instantiate the argparser (for reading the command line arguments).
    parser = argparse.ArgumentParser(description='Assembler for DLC')
    # Adding the qrguments.
    parser.add_argument('-f',
                        type=str,
                        help='Input assembly file')
    parser.add_argument('-o',
                        type=str,
                        help='Output file')
    parser.add_argument('-t',
                        help='Test; will compile the input file and print the result to the screen instead of writing it to a file',
                        action='store_true')

    # Parse the given arguments.
    argsp = parser.parse_args()

    # Stop the program when no input file is specified.
    # TODO: Print short help message here
    if argsp.f is None:
        print('Fatal: No input file specified')
        exit()

    # LUT for the instruction set
    ins_set = {
        'NOP': 0x0,
        'JMP': 0x1,
        'JPO': 0x2,
        'OUT': 0x3,
        'LDA': 0x4,
        'SWP': 0x5,
        'INV': 0x6,
        'JMZ': 0x7,
        'JME': 0x8,
        'NND': 0x9,
        'NOR': 0xa,
        'XOR': 0xb,
        'ADD': 0xc,
        'SUB': 0xd,
        'ADS': 0xe,
        'SBS': 0xf
    }
    # Create an empty string for the file content.
    content = ""
    # Use a context manager for opening (and reading) the input file.
    with open(argsp.f, 'r') as input_file:
        content = input_file.read()

    # Split the file on the newline character.
    content = content.split('\n')

    # Parse the lines and remove the comments.
    for index, line in enumerate(content):
        if ';' in line:
            com_in = line.find(';')
            content[index] = line[0:com_in].rstrip()

    # Create an empty dictionary for the labels & literals (constants).
    labels = {}
    literals = {}

    # Start at the beginning of the memory.
    memory_index = 0
    # Create a list representing the memory (16 zero's).
    memory = [0x0]*16

    # Go over each line and convert it to bytecode.
    for index, line in enumerate(content):

        if not line:
            # If the line is empty skip it, and go to the next line.
            pass

        elif line[0] == '.':
            # if the line starts with a '.' it is a label, which doesn't need to be written to the memory.

            # Remove the dot from the name.
            label = line[1:]

            if label in labels.keys():
                # If the label has been previously defined, print and exit.
                print('Fatal: redefining existing label at line: {}'.format(index + 1))
                exit()
            else:
                # Otherwise add the label to the dictionary with the current memory index, for future reference.
                labels[label] = memory_index

        else:
            # In all other case we have an opcode with arguments

            # Extract the tokens from the line and remove leading whitespace.
            tokens = line.lstrip().split(' ')
            # Take the opcode (being the first token).
            opcode = tokens[0]

            if opcode in ins_set.keys():
                # If the opcode is in the instruction set.

                # Write the opcode to the memory.
                memory[memory_index] = ins_set[opcode]
                # Increment the memory index.
                memory_index += 1

                if opcode in ['NOP', 'OUT', 'SWP', 'INV', 'ADS', 'SBS'] and len(tokens) != 1:
                    # If the opcodes that take no arguments but receive one or more, print and exit
                    print('Fatal: {} opcode takes no arguments, {} where given on line: {}'.format(
                        opcode, len(tokens)-1, index))
                    exit()

                elif len(tokens) > 1:
                    # For the other arguments, write there arguments to the memory.

                    # All opcodes take at most one argument, so we don't need to check if they have more.
                    arg = tokens[1]

                    if arg[0] == "#":
                        # If the arg starts with '#', it is a constant.

                        # Convert the arg from a string to an integer.
                        arg = int(arg[1:], 16)

                        if arg not in literals.keys():
                            # If the constant is not yet in the list of constants, add it.

                            # Get the constants already in the list
                            values = list(literals.values())

                            if values:
                                # If there are already values in the list, add it before the first address.
                                # Constants are added from last memory address to the front
                                literals[arg] = values[-1] - 1
                            else:
                                # If there are no constants in the list yet, add it to the bask.
                                literals[arg] = 0xf

                            # Write the constant to the memory.
                            memory[literals[arg]] = arg

                        # Write the address of the constant to the memory
                        memory[memory_index] = literals[arg]
                        # Increment the memory address
                        memory_index += 1

                    elif arg[0] == "$":
                        # if the arg starts with '$', it is a literal memory address.
                        # Decode it and write it the the memory.
                        memory[memory_index] = int(arg, 16)
                        memory_index += 1

                    else:
                        # If the argument is not a constant or memory address, check if it is a label.
                        if arg in labels.keys():
                            # If it is a label add the correseponding value to the memory.
                            memory[memory_index] = labels[arg]
                            memory_index += 1
                        else:
                            # Otherwise it is an invalid argument, print and exit.
                            print(
                                'Fatal: invalid argument at line: {}'.format(index + 1))
                            exit()

            else:
                # If the opcode is not in the instruction set, print and exit.
                print('Fatal: unknown token on line: {}'.format(index+1))
                exit()

    # If the test flag is given, just print the memory to the screen.
    if argsp.t:
        print(memory)
    else:
        # If no output file is specified use the default name, otherwise use the given one.
        if argsp.o is None:
            output_file = argsp.f.split('.')[0] + ".bin"
        else:
            output_file = argsp.o
        # Other wise write the memory to a file.
        with open(output_file, 'wb') as output:
            output.write(bytes(memory))
