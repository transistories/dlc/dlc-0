# Contributing to Transistories

This is a personal project, so it is impossible for me to do everything, or think of everything, myself.
I love any kind of feedback or input, and want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing (or implementing) new features
- Becoming a maintainer
- Something else entirely

## I develop using Gitlab

I use my own (self-hosted) instance of Gitlab to host code, track issues and feature requests as well as accept merge requests.

## About workflow

1. create a separate branch to work on (you can start from any branch you like)
1. If you have added code that should be tested, add tests
1. If you have changed API's or interfaces, update the documentation.
1. Ensure that existing tests still pass.
1. Make sure that your code lints and adheres to the existing code style.
1. Create a merge request, preferably into the `develop` branch.

## Report bugs using Gitlab's issues

We use Gitlab issues to track bugs. Report a bug by [opening a new issues](https://git.transistories.org/transistories/dlc/dlc-0/-/issues/new "Create a new issue"); it is that easy.

## how to bug report

// TODO :D But be complete

## Styling

// TODO :D

## License

The project is licensed under [GNU GPL V3.0](LICENSE.md "The license text.").
By contributing, you agree that your contributions will be licensed under the same license. In the same line it is understood that when you add or change code that your submission is made under the same license. You can contact one of the maintainers if that is a concern.

## References

This document was based on and adapted from [briandk/CONTRIBUTING.md](https://gist.github.com/briandk/3d2e8b3ec8daf5a27a62 "Source document")
